const mongoose = require('./db')


//定义商品数据表
const ProductSchema = mongoose.Schema({
    name:String,          //商品名称
    parmams:{             //商品参数
        type:Object,
        default:{
            date: '2098年05月06日 至 2098年05月08日',
            barnd: '小默制药',
            cate: '铁皮石斛',
            package: '礼盒装',
            sign: '保健食品(食健字)',
            isSugar: '加糖'
        }
    },
    newPrice:Number,      //新价
    oldPrice:Number,      //旧价
    img:String,        //图片路径
    spec:{                //商品规格
        type:Array,
        default:['预留规格1','预留规格2']
    },
    
    imgs:{                 //详情页多图说明
        type:Array,
        default:['http://127.0.0.1:3000/imgs/product/1.jpg',
            'http://127.0.0.1:3000/imgs/product/2.jpg',
            'http://127.0.0.1:3000/imgs/product/3.jpg',
            'http://127.0.0.1:3000/imgs/product/4.jpg'
        ]
    },
    stock:Number,           //库存
    sales:Number,           //销量
    cate:String,            //商品分类
    cateid:String,          //分类id
    isCollect:{              //收藏的用户id
        type:Array,
        default:[]
    }
})

//定义商品分类表
const CateSchema = mongoose.Schema({
    name:String,        //分类名称
    id:String,          //分类id
})

//定义购物车表格
const CartSchema = mongoose.Schema({
    userid:String,      //用户id
    imgUrl:String,      //图片路径
    name:String,        //商品名称
    count:Number,       //商品数量
    checked:Boolean,    //是否选中
    newPrice:Number,    //新价
    oldPrice:Number     //旧价
})



const ProductModel = mongoose.model('product',ProductSchema,'product')

const CateModel = mongoose.model('cate',CateSchema,'cate')

const CartModel = mongoose.model('cart',CartSchema,'cart')

module.exports={
    ProductModel,
    CateModel,
    CartModel
}