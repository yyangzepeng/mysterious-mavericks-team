const mongoose = require("./db")
const userShema = mongoose.Schema({
    name:String,
    password:String,
    age:Number,
    sex:Number,
    phone:String,
    email:String,
})
const userModel = mongoose.model('user',userShema,'user')


module.exports = {
    userModel
}