const mongoose = require('./db')

let Schema = new mongoose.Schema({
    name: String,
    password: String,
    age: Number,
    sex: Number,
    phone: String,

})
let User = mongoose.model('user1', Schema,'user1')

let Commodity = new mongoose.Schema({
    name: String, //商品名称
    price: Number,  //商品价格
    img: String, //商品图片
    desc: String, //商品描述
    num: Number,    //商品数量
    type: String, //商品类型
    order:Number,//商品订单号
    flag:{type:Boolean,default:false}, //商品是否付款
    refund:{type:Boolean,default:false}, //商品是否被退款
    shpr:{type:Boolean,default:false}, //商品是否被发货
    favorite:{type:Boolean,default:false}, //商品是否被收藏
    
})
let commodity = mongoose.model('commodity', Commodity,'commodity')
module.exports={
    User,
    commodity
}