var express = require('express');
var router = express.Router();
var { User, commodity } = require('../model/modelyg');
/* GET home page. */

// 获取用户
router.get('/getuser', async (req, res) => {
    let { name, password } = req.query
    let data = await User.find({ name, password })
    res.send({
        code: 200,
        msg: "获取成功",
        data: data
    })
})
//获取商品数据
router.get('/getcommodity', async (req, res) => {
    let data = await commodity.find()
    res.send({
        code: 200,
        msg: "获取成功",
        data: data
    })
})
// 添加商品
router.post('/addcommodity', (req, res) => {
    commodity.create(req.body)
    res.send({
        code: 200,
        msg: "添加成功",
    })
})
//搜索商品
router.get('/searchcommodity', async (req, res) => {
    let { name } = req.query
    console.log(name);
    let arr = []
    let title = new RegExp(name)
    if (title) { arr.push({ name: title }) }
    let data = await commodity.find({ $and: arr })
    res.send({
        code: 200,
        msg: "获取成功",
        data: data
    })
})


// 注销用户
router.get("/clearuser", async (req, res) => {
    let { id } = req.query
    await User.deleteOne({ _id: id })
    res.send({
        code: 200,
        msg: "注销成功",
    })
})
//修改密码
router.get("/updatepassword", async (req, res) => {
    let { id, password } = req.query
    await User.updateOne({ _id: id }, { password })
    res.send({
        code: 200,
        msg: "修改成功",
    })
})


module.exports = router;