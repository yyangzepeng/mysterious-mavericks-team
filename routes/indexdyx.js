var express = require('express');
var router = express.Router();
var {homeModel} = require('../model/modeldyx');
/* GET home page. */


//获取首页数据
router.get('/home',async (req,res)=>{
    let {name}=req.query;
    // console.log(req.query);
    let name_reg = new RegExp(name);  // 加上 'i' 表示忽略大小写
    let data = await homeModel.find({ name: name_reg });
    // console.log(data.length);
    res.send({
        code:0,
        data,
        msg:'获取成功'
    })
})
 
module.exports = router;