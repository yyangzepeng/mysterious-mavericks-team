var express = require('express');
var router = express.Router();
var {ProductModel,CateModel,CartModel} = require('../model/modeldmx');


//获取商品分类
router.get('/cate', async(req, res)=> {
  const cates =await CateModel.find({})
  res.send({
    code:200,
    data:cates
  })
});

//获取所有商品
router.get('/product', async(req, res)=> {
  const products =await ProductModel.find({})
  res.send({
    code:200,
    data:products
  })
});

//获取单个商品
router.get('/product/:id', async(req, res)=> {
  const product =await ProductModel.findById(req.params.id)
  res.send({
    code:200,
    data:product
  })
});






module.exports = router;